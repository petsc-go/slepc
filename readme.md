SLEPc
=====

A [Go](http://golang.org/) package for calling functions from
[SLEPc](http://www.grycap.upv.es/slepc/) (Scalable Library for Eigenvalue
Problem Computations).

License
-------

Unless otherwise stated, this package is to be considered an original work
and available under the terms of the
[2-clause BSD license](http://opensource.org/licenses/BSD-2-Clause).

The examples provided with this package were translated from those included
with SLEPc. They are therefore distributed under the terms of the
[GNU Lesser General Public License Version 3](http://www.gnu.org/licenses/lgpl-3.0.txt).