SLEPc Examples
==============

Examples of calling functions from [SLEPc](http://www.grycap.upv.es/slepc/)
(Scalable Library for Eigenvalue Problem Computations) using the
[Go](http://golang.org/) programming language.

  - [Standard Symmetric Eigenvalue Problem](./ex1eps/ex1eps.go)

License
-------

The examples provided by this package were translated from those included
with SLEPc. They are therefore distributed under the terms of the
[GNU Lesser General Public License Version 3](./lgpl-3.0.txt).