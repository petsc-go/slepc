// SLEPc - Scalable Library for Eigenvalue Problem Computations
// Copyright (c) 2002-2013, Universitat Politecnica de Valencia, Spain
//
// This file is part of SLEPc.
//
// SLEPc is free software: you can redistribute it and/or modify it under the
// terms of version 3 of the GNU Lesser General Public License as published by
// the Free Software Foundation.
//
// SLEPc is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY;  without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with SLEPc. If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"github.com/rwl/slepc"
	"github.com/rwl/petsc"
)

const help = `Standard symmetric eigenproblem corresponding to the Laplacian operator in 1 dimension.\n
The command line options are:
  -n <n>, where <n> = number of grid subdivisions = matrix dimension.\n\n`

func main() {
	value [3]float64
	col := [3]int{}
	FirstBlock := false
	LastBlock := false

	slepc.Initialize(os.Args, "", help)

	n, set := petsc.OptionsGetInt("", "-n")
	if !set {
		n = 30
	}
	petsc.Printf(petsc.CommWorld, "\n1-D Laplacian Eigenproblem, n=%D\n\n", n)

	// Compute the operator matrix that defines the eigensystem, Ax=kx
	A := petsc.NewMat(petsc.CommWorld)
	A.SetSizes(petsc.Decide, petsc.Decide, n, n)
	A.SetFromOptions()
	A.SetUp()

	Istart, Iend := A.OwnershipRange()
	if Istart == 0 {
		FirstBlock = true
	}
	if Iend == n {
		LastBlock = true
	}
	value[0] = -1.0
	value[1] =  2.0
	value[2] = -1.0
	var i int
	if FirstBlock {
		i = Istart+1
	} else {
		i = Istart
	}
	var n int
	if LastBlock {
		n = Iend-1
	} else {
		n = Iend
	}
	for ; i < n; i++ {
		col[0] = i-1
		col[1] = i
		col[2] = i+1
		A.SetValues([]int{i}, col, value, petsc.InsertValues)
	}
	if LastBlock {
		i = n-1
		col[0] = n-2
		col[1] = n-1
		A.SetValues([]int{i}, col, value, petsc.InsertValues)
	}
	if FirstBlock {
		i = 0
		col[0] = 0
		col[1] = 1
		value[0] = 2.0
		value[1] = -1.0
		A.SetValues([]int{i}, col, value, petsc.InsertValues)
	}

	A.AssemblyBegin(petsc.MatFinalAssembly)
	A.AssemblyEnd(petsc.MatFinalAssembly)

	xr := A.Vecs(nil)
	xi := A.Vecs(nil)

	// Create the eigensolver and set various options.

	// Create eigensolver context.
	eps := slepc.NewEPS(petsc.CommWorld)

	// Set operators. In this case, it is a standard eigenvalue problem.
	eps.SetOperators(A, nil)
	eps.SetProblemType(slepc.EPS_HEP)

	// Set solver parameters at runtime.
	eps.SetFromOptions()

	// Solve the eigensystem.
	eps.Solve()

	// Optionally get some information from the solver and display it.
	its := eps.IterationNumber()
	petsc.Printf(petsc.CommWorld, " Number of iterations of the method: %D\n", its)
	typ := eps.Type()
	petsc.Printf(petsc.CommWorld, " Solution method: %s\n\n", typ)
	nev, _, _ := eps.Dimensions()
	petsc.Printf(petsc.CommWorld, " Number of requested eigenvalues: %D\n", nev)
	tol, maxit := eps.Tolerances()
	petsc.Printf(petsc.CommWorld, " Stopping condition: tol=%.4G, maxit=%D\n", tol, maxit)

	// Display solution and clean up.

	// Get number of converged approximate eigenpairs.
	nconv := eps.Converged()
	petsc.Printf(petsc.CommWorld, " Number of converged eigenpairs: %D\n\n", nconv)

	if nconv > 0 {
		// Display eigenvalues and relative errors.
		petsc.Printf(petsc.CommWorld,
		"           k          ||Ax-kx||/||kx||\n" +
		"   ----------------- ------------------\n")

		for i = 0; i < nconv; i++ {
			// Get converged eigenpairs: i-th eigenvalue is stored in kr (real part) and
			// ki (imaginary part)
			kr, ki := eps.Eigenpair(i, xr, xi)

			// Compute the relative error associated to each eigenpair
			err := eps.RelativeError(i)

			re = kr
			im = ki
			if im != 0.0 {
				petsc.Printf(petsc.CommWorld, " %9F%+9F j %12G\n", re, im, err)
			} else {
				petsc.Printf(petsc.CommWorld, "   %12F       %12G\n", re, err)
			}
		}
		petsc.Printf(petsc.CommWorld, "\n")
	}

	// Free work space.
	eps.Destroy()
	A.Destroy()
	xr.Destroy()
	xi.Destroy()

	slepc.Finalize()
}
